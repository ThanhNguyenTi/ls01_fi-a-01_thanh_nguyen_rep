import java.util.Scanner;

public class Summe {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int summeBis = 0;
		int summe = 0;
		int zaehler = 0;


		System.out.println("Geben Sie eine Zahl ein, bis welche addiert werden soll.");
		summeBis = sc.nextInt();
		
		//kopfgesteuerte Schleife
		
		while(zaehler <= summeBis) {
			summe = summe + zaehler;
			zaehler++;
		}
		
		System.out.printf("Mit der kopfgesteuerten Schleife: %d", summe);
		
		summe = 0;
		zaehler = 0;
		
		//Fußgesteuerte Schleife
		
		do {
			summe = summe + zaehler;
			zaehler++;
		}
		
		while(zaehler <= summeBis);
		
		System.out.printf("\n Mit der fußgestellten Schleife Schleife: %d", summe);
		
		summe = 0;
		
		int i;
		for(i = 0; i <= 10; i++); {
			summe = summe + i;
		}
		System.out.printf("\nDas ist so lol: %d", summe);
	}

}
