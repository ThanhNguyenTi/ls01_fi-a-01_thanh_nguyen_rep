import java.util.Scanner;

public class zählen {

	public static void main(String[] args) {

		int x;
		
			Scanner myScanner = new Scanner(System.in);
			System.out.print("Bitte geben Sie eine natürliche Zahl ein: ");
			x = myScanner.nextInt();

			System.out.println("Hochgezählt:");
			hoch(x);

			System.out.println("\nRuntergezählt:");
			runter(x);

	}

	public static void hoch(int F) {
		for (int i = 1; i <= F; i++) {
			System.out.println(i);
		}
	}

	public static void runter(int F) {
		for (int i = F; i > 0; i--) {
			System.out.println(i);

		}
	}
}
