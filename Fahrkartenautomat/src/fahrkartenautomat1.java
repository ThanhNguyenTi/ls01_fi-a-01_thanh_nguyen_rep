import java.util.Scanner;

class fahrkartenautomat1 {

    public static void main(String[] args) {
    	
    	boolean repeat = true;
        do{
        	
        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        rueckgeldAusgeben(rueckgabebetrag);
    		
        }while(repeat);
     }
    public static double fahrkartenbestellungErfassen() {
    	
    	double ticketpreis = 0;
    	int b;
        double zuZahlenderBetrag;
        int ticketArt = 0;
        int anzahlTickets;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        System.out.println(" Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
        System.out.println(" Tageskarte Regeltarif AB [8,60 EUR] (2)");
        System.out.println(" Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
        System.out.println("\n");
        System.out.println("Ihre Wahl:");
        b = in.nextInt();
       
        while( 4 <= b)  {
   			System.out.println("Ungültige Wahl! Bitte wählen Sie 1,2 oder 3.");
   			b = in.nextInt();
        }
        
        if(b == 1){
 			ticketpreis = 2.9;
 		}
         else if(b == 2){
 			ticketpreis = 8.6;
 		}
 		else if(b == 3){
 			ticketpreis = 23.5;
 		}
        
        System.out.print("Anzahl der Tickets:");
        anzahlTickets = in.nextInt();
        
        while (anzahlTickets <= 0 || anzahlTickets > 10) {
            System.out.println("Ungültige Anzahl! Bitte wählen Sie eine Ticketanzahl zwischen 0 und 10 aus: ");
            anzahlTickets = in.nextInt();
        	}
        zuZahlenderBetrag = ticketpreis * anzahlTickets;
        return zuZahlenderBetrag;
    }
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
        Scanner in = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMuenze;

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f €%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = in.nextDouble();
            
            if (eingeworfeneMuenze == 0.05){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            } 
            else if (eingeworfeneMuenze == 0.1){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            }  
            else if (eingeworfeneMuenze == 0.2){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            }  
            else if (eingeworfeneMuenze == 0.5){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            }  
            else if (eingeworfeneMuenze == 1){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            } 
            else if (eingeworfeneMuenze == 2){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            } 
            else {
                System.out.println("Dies ist kein akzeptiertes Zahlungsstück.");
            }

        }
        return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {
        if (rueckgabebetrag > 0.0) {
            System.out.format("Der Rückgabebetrag in Höhe von %1.2f € %n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");
            while (rueckgabebetrag >= 2.0)
            {
                System.out.println("2 Euro");
                rueckgabebetrag -= 2.0;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
            while (rueckgabebetrag >= 1.0)
            {
                System.out.println("1 Euro");
                rueckgabebetrag -= 1.0;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
            while (rueckgabebetrag >= 0.5)
            {
                System.out.println("50 Cent");
                rueckgabebetrag -= 0.5;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
            while (rueckgabebetrag >= 0.2)
            {
                System.out.println("20 Cent");
                rueckgabebetrag -= 0.2;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
            while (rueckgabebetrag >= 0.1)
            {
                System.out.println("10 Cent");
                rueckgabebetrag -= 0.1;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
            while (rueckgabebetrag >= 0.05)
            {
                System.out.println("5 Cent");
                rueckgabebetrag -= 0.05;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
        }
        System.out.println("\nVergessen Sie nicht, den Fahrschein"+
                " vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.\n\n");
    		}
    }