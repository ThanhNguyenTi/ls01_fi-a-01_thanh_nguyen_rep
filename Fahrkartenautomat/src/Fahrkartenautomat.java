﻿import java.util.Scanner;

class Fahrkartenautomat
{

    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       //Deklaration der Variablen
       
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       int anzahl;

       
       System.out.printf("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextFloat();

       System.out.printf("Anzahl der Tickets:");
       anzahl = tastatur.nextInt();
       
     
           
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag*anzahl)
       {
    	   System.out.printf("Noch zu zahlen: %1.2f Euro\n", ((zuZahlenderBetrag*anzahl) - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
       
     //Methode fahrkartenBezahlen
     //--------------------------
     //public void bezahlen	(double betrag){
     //     eingeworfeneMünze += betrag;
     //}
    
       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag*anzahl;
       if(rückgabebetrag >= 0.00)
       {
    	   System.out.println("Der Rückgabebetrag beträgt " + rückgabebetrag + " Euro");
    	   System.out.println("Es wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 1.99) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 1.99;
           }
           while(rückgabebetrag >= 0.99) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 0.99;
           }
           while(rückgabebetrag >= 0.49) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.49;
           }
           while(rückgabebetrag >= 0.19) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.19;
           }
           while(rückgabebetrag >= 0.09) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.09;
           }
           while(rückgabebetrag >= 0.049)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.049;
           }
           if(rückgabebetrag >= 0.00)// 0 Euro
           {
        	  System.out.println("Nichts");
 	          rückgabebetrag -= 0;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein"+
                          " vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}