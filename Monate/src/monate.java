import java.util.Scanner;

public class monate {

	public static void main(String[] args) {

		Scanner Tastatur = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine Zahl ein, um den dazugehörigen Monat zu finden:");
		int zahlmonat = Tastatur.nextInt();
		
		switch (zahlmonat) {
			case 1:
				System.out.println("Diese Zahl ist der Monat Januar");
			    break;
			case 2:
				System.out.println("Diese Zahl ist der Monat Februar");
			    break;
			case 3:
				System.out.println("Diese Zahl ist der Monat März");
			    break;
			case 4:
				System.out.println("Diese Zahl ist der Monat April");
			    break;
			case 5:
				System.out.println("Diese Zahl ist der Monat Mai");
			    break;
			case 6:
				System.out.println("Diese Zahl ist der Monat Juni");
			    break;
			case 7: 
				System.out.println("Diese Zahl ist der Monat Juli");
			    break;
			case 8: 
				System.out.println("Diese Zahl ist der Monat August");
			    break;
			case 9:
				System.out.println("Diese Zahl ist der Monat September");
			    break;
			case 10:
				System.out.println("Diese Zahl ist der Monat Oktober");
			    break;
			case 11: 
				System.out.println("Diese Zahl ist der Monat November");
			    break;
			case 12: 
				System.out.println("Diese Zahl ist der Monat Dezember");
			    break;
			    
			default:
				System.out.println("Es gibt keinen Monat zu dieser Zahl");
		}
	}
}
