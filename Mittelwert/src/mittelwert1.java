		import java.util.Scanner;

		public class mittelwert1 {

		   public static void main(String[] args) {

		      // (E) "Eingabe"
		     
			  System.out.print("Wie lang soll das Array sein?");
		      Scanner scan = new Scanner(System.in);
		      
		      int [] zahlen = 
		      // (V) Verarbeitung

		      m = berechneMittelwert(x, y);
		      
		      // (A) Ausgabe

		      ausgabe (x, y, m);
		            
		   }
		   
		   
		   // Methode Verarbeitung
		   public static double berechneMittelwert(double x1, double x2) {
		      
		      double mittelwert;
		      
		      mittelwert = (x1 + x2) / 2.0;
		      
		      return mittelwert;
		   }
		   
		   
		   // Methode Ausgabe
		   public static void ausgabe (double x, double y, double m) {
			   
			   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
			   
		   }
	}

