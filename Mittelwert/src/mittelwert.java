
import java.util.Scanner;

public class mittelwert {
	
	public static void main(String[] args) {

		//Deklaration und Initialisierung der Variablen
		double zahl1 = 0.0;
		double zahl2 = 0.0;
		double zahl3 = 0.0;
		double ergebnis2, ergebnis3;
		
		Scanner myScanner = new Scanner(System.in);
		
		//(E) "Eingabe"
		//Werte für x und y festlegen:
		//===========================
		programmHinweis();
		System.out.print("Geben Sie die erste Zahl ein:");
		zahl1 = myScanner.nextDouble();
		
		System.out.print("Geben Sie die zweite Zahl ein:");
		zahl2 = myScanner.nextDouble();
		
		System.out.print("Geben Sie die dritte Zahl ein:");
		zahl3 = myScanner.nextDouble();
		
		//(V) Verarbeitung
		// Mittelwert von x und y berechnen:
		//=================================
		ergebnis2 = mittelwertBerechnung (zahl1, zahl2);
		
		ergebnis3 = mittelwertBerechnung (zahl1, zahl2, zahl3);
		
		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		//===================================
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2,ergebnis2);
		
		System.out.printf("Der Mittelwert von %.2f, %.2f und %.2f ist %.2f\n", zahl1, zahl2, zahl3,ergebnis3);
		myScanner.close();
	}
		static double mittelwertBerechnung(double zahlEins, double zahlZwei) {
		//double ergebnisMethode = 0.0;
		//ergebnisMethode = zahlEins + zahlZwei) / 2.0;
		//return ergebnisMethode;

		return (zahlEins + zahlZwei) / 2.0;

		 }

		static double mittelwertBerechnung(double zahlEins, double zahlZwei, double zahlDrei)  {
		//double ergebnisMethode = 0.0;
		//ergebnisMethode = zahlEins + zahlZwei + zahlDrei) / 3.0;
		//return ergebnisMethode;

		return (zahlEins + zahlZwei + zahlDrei) /3.0;

		 }

		static void programmHinweis() {
			System.out.println("Dieses Programm ermittelt den Mittelwert von zwei oder drei Zahlen");
	   }

}
