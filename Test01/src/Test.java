import java.util.ArrayList;

public class Test
{
    //Deklaration der Instanzvariablen
    double fahrPreis;
    double einwurf;
    double gesamtEinnahmen;
    ArrayList<Double> ticketumsaetze = new ArrayList<Double>();

    //Konstruktor
    public void Fahrkartenautomat(){
        //Initialisierung der Instanzvariablen
        fahrPreis = 3.8;
        einwurf = 0.0;
        gesamtEinnahmen = 0.0;
        ticketumsaetze.clear();
    }

    //Methode zum Festlegen des Fahrpreises
    public void fahrpreisFestlegen(double Fahrpreis){
        fahrPreis = Fahrpreis;
    }

    //Methode zum Einzahlen
    public void einzahlen(double betrag){
        einwurf += betrag;
    }

    //Methode zum Drucken der Fahrkarte
    public void druckeFahrkarte(){
        System.out.println("Fahrpreis: " + fahrPreis + "€");
        System.out.println("Bereits gezahlt: " + einwurf + "€");
        if (einwurf < fahrPreis){
            System.out.println("Es fehlen noch " + (fahrPreis - einwurf) + "€.");
        } else if (einwurf == fahrPreis){
            System.out.println("Der Kauf wurde abgschlossen.");
            gesamtEinnahmen += fahrPreis;
            ticketumsaetze.add(fahrPreis);
            einwurf = 0.0;
        } else if (einwurf > fahrPreis){
            System.out.println("Der Kauf wurde abgschlossen.");
            gesamtEinnahmen += fahrPreis;
            ticketumsaetze.add(fahrPreis);
            System.out.println("Sie erhalten " + (einwurf - fahrPreis) + "€ Wechselgeld.");
            einwurf = 0.0;
        }
    }

    //Methode zum Anzeigen der Gesamteinnahmen
    public double getGesamteinnahmen(){
        return gesamtEinnahmen;
    }

    //Methode zum Abbrechen
    public void abbrechen(){
        System.out.println("Der Vorgang wird abgebrochen. Sie erhalten " + einwurf + "€ zurück.");
        einwurf = 0.0;
        fahrPreis = 0.0;
    }

    //Methode zum Ausdrucken des Ticketverlaufes
    public void ticketkaufverlaufAusdrucken(){
        System.out.println("\n--------------------------------------------");
        System.out.println("Ihre Umsätze: ");
        System.out.println("--------------------------------------------");
        for (int i = 0; i < ticketumsaetze.size(); i++){
            System.out.println("Ticketverkauf für: " + ticketumsaetze.get(i));
            System.out.println("--------------------------------------------");
        }
        System.out.println("Gesamteinnahmen bisher: " + getGesamteinnahmen() + "\n");
        System.out.println("--------------------------------------------");
    }
}

