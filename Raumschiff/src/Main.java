/**
 * @author Kevin Thanh
 *
 */
public class Main {


	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2);
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni'Var", 5);
		
		
		klingonen.zustandRaumschiffAusgeben();
		
		Ladung ladungKlingonen1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladungKlingonen2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		klingonen.beladenMitLadungen(ladungKlingonen1);
		klingonen.ladungsVerzeichnisAusgeben();
		klingonen.beladenMitLadungen(ladungKlingonen2);
		klingonen.ladungsVerzeichnisAusgeben();
		
		
		romulaner.zustandRaumschiffAusgeben();
		
		Ladung ladungRomulaner1 = new Ladung("Borg-Schrott", 5);
		Ladung ladungRomulaner2 = new Ladung("Rote Materie", 2);
		Ladung ladungRomulaner3 = new Ladung("Plasma-Waffe", 50);
		romulaner.beladenMitLadungen(ladungRomulaner1);
		romulaner.ladungsVerzeichnisAusgeben();
		romulaner.beladenMitLadungen(ladungRomulaner2);
		romulaner.ladungsVerzeichnisAusgeben();
		romulaner.beladenMitLadungen(ladungRomulaner3);
		romulaner.ladungsVerzeichnisAusgeben();
		
		vulkanier.zustandRaumschiffAusgeben();
		
		Ladung ladungVulkanier1 = new Ladung("Forschungssonde", 35);
		Ladung ladungVulkanier2 = new Ladung("Photonentorpedo", 3);
		vulkanier.beladenMitLadungen(ladungVulkanier1);
		vulkanier.ladungsVerzeichnisAusgeben();
		vulkanier.beladenMitLadungen(ladungVulkanier2);
		vulkanier.ladungsVerzeichnisAusgeben();
		
		System.out.println("\n"+ "------------------------------------");
		klingonen.schiessePhotonentorpedos(romulaner);
		romulaner.schiessePhaserkanonen(klingonen);
		vulkanier.sendeNachrichtAnAlle("Vulkanier: Gewalt ist nicht logisch.");
		System.out.println("------------------------------------");
		
		klingonen.zustandRaumschiffAusgeben();
		klingonen.ladungsVerzeichnisAusgeben();
		
		System.out.println("\n"+ "------------------------------------");
		klingonen.schiessePhotonentorpedos(romulaner);
		klingonen.schiessePhotonentorpedos(romulaner);
		
		System.out.println("------------------------------------");
		klingonen.zustandRaumschiffAusgeben();
		klingonen.ladungsVerzeichnisAusgeben();
		romulaner.zustandRaumschiffAusgeben();
		romulaner.ladungsVerzeichnisAusgeben();
		vulkanier.zustandRaumschiffAusgeben();
		vulkanier.ladungsVerzeichnisAusgeben();
		System.out.println("------------------------------------");
	}
}
