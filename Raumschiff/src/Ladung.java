/**
 * @author Kevin Thanh
 *
 */
public class Ladung {

	private String bezeichnung;
	private int anzahl;
	
	public Ladung() {
		super();
	}
	
	/**
	 * Vollparametrisierter Konstruktor f�r die Klasse Raumschiff.
	 * 
	 * @param bezeichnung
	 * @param anzahl
	 */
	public Ladung (String bezeichnung, int anzahl) {
		super();
		this.bezeichnung = bezeichnung;
		this.anzahl = anzahl;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	
}
