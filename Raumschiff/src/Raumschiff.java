import java.util.ArrayList;

/**
 * @author Kevin Thanh
 *
 */

public class Raumschiff {

	private String name;
	private int energieversorgungInProzent;
	private int schutzschildeInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int huelleInProzent;
	private int photonentorpedos;
	private int reparaturAndroid;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> Ladungsverzeichnis;		

	public Raumschiff() {
		super();
	}
	
	/**
	 * Vollparametrisierter Konstruktor f�r die Klasse Raumschiff.
	 * 
	 * @param photonentorpedos
	 * @param energieversorgungInProzent
	 * @param schutzschildeInProzent
	 * @param huelleInProzent
	 * @param lebenserhaltungssystemeInProzent
	 * @param name
	 * @param reparaturAndroid
	 */
	public Raumschiff(int photonentorpedos,  int energieversorgungInProzent, int schutzschildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String name, int reparaturAndroid) {
		super();
		this.name = name;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schutzschildeInProzent = schutzschildeInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.huelleInProzent = huelleInProzent;;
		this.photonentorpedos = photonentorpedos;
		this.reparaturAndroid = reparaturAndroid;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchutzschildeInProzent() {
		return schutzschildeInProzent;
	}

	public void setSchutzschildeInProzent(int schutzschildeInProzent) {
		this.schutzschildeInProzent = schutzschildeInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getPhotonentorpedos() {
		return photonentorpedos;
	}

	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}

	public int getReparaturAndroid() {
		return reparaturAndroid;
	}

	public void setReparaturAndroid(int reparaturAndroid) {
		this.reparaturAndroid = reparaturAndroid;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return Ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		Ladungsverzeichnis = ladungsverzeichnis;
	}
	
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	/**
	 * Wenn Photonentorpedos <= 0 ist, dann wird "Click" ausgegeben;
	 * Wenn Photonentorpedos => 0 ist, dann wird Photonentorpedo abgeschossen, eine Nachricht gesendet und die Anzahl um eins verringert;
	 * Au�erdem wird die Methode Treffer aufgerufen;
	 * 
	 * @param Photonentorpedos schie�en
	 */
	public void schiessePhotonentorpedos(Raumschiff raumschiff) {
		if(this.getPhotonentorpedos() <= 0) {
			System.out.println("-=*Click*=-");
		} else {
			this.setPhotonentorpedos(photonentorpedos-1);
			sendeNachrichtAnAlle("Photonentorpedo abgeschossen.");
			vermerkeTrefferAufEigenesSchiff(raumschiff);
		}
    }

    /**
     * Ist Energieversorgung kleiner als 50% so wird "Click" ausgegeben;
     * Ist Energieversorgung gr��er als 50% so wird Phaserkanone abgeschossen und eine Nachricht gesendet;
     * Au�erdem wird die Methode Treffer aufgerufen;
     * 
     * @param Phaserkanonen schie�en
     */
    public void schiessePhaserkanonen(Raumschiff raumschiff) {
    	if(this.getEnergieversorgungInProzent() < 50) {
			System.out.println("=*Click*=-");
		} else {
			this.setEnergieversorgungInProzent(energieversorgungInProzent-50);
			sendeNachrichtAnAlle("Phaserkanone abgeschossen.");
			vermerkeTrefferAufEigenesSchiff(raumschiff);
		}
    }

    /**
     * Nachricht "[Schiffsname] wurde getroffen!" wird in der Konsole ausgegeben
     * 
     * @param Treffer vermerken
     */
    private void vermerkeTrefferAufEigenesSchiff(Raumschiff raumschiff) {
    		System.out.println(raumschiff.getName() + " wurde getroffen!");
    		raumschiff.setSchutzschildeInProzent(raumschiff.getSchutzschildeInProzent()-50);
    		if(raumschiff.getSchutzschildeInProzent() <= 0) {
    			raumschiff.setHuelleInProzent(raumschiff.getHuelleInProzent()-50);
    			raumschiff.setEnergieversorgungInProzent(raumschiff.getEnergieversorgungInProzent()-50);
    			if(raumschiff.getHuelleInProzent() <= 0) {
    				raumschiff.setLebenserhaltungssystemeInProzent(0);
    				sendeNachrichtAnAlle("Lebenserhaltungssysteme sind zerst�rt.");
    			}
    		}
    }

    /**
     * Nachricht wird in der Konsole ausgegeben
     * 
     * @param nachricht an alle senden
     */
    public void sendeNachrichtAnAlle(String nachricht) {
    	System.out.println(nachricht);
    }

    /**
     * @param ladung beladen
     */
    public void beladenMitLadungen(Ladung ladung) {
    	ArrayList<Ladung> lad = new ArrayList<Ladung>();
		lad.add(ladung);
		this.setLadungsverzeichnis(lad);
    }

    /**
     * @return broadcastKommunikator
     */
    public ArrayList<String> logbuchAusgeben() {
    	return this.getBroadcastKommunikator();
    }

    public void beiPhotonentorpedosInRohreLaden(int anzahlTorpedos) {
    	
	}

    public void repariereSchildeHuelleEnergieversorgungVonAndroiden(boolean schutzschilde,
            boolean energieversorgung, boolean schiffshuelle, int anzahlDerAndroiden) {
        // TODO
    }

    /**
     *  Zustand des Raumschiffs auf der Konsole ausgeben
     */
    public void zustandRaumschiffAusgeben() {
        System.out.println("\n"+"Name: " + this.getName() + "\n" + "Energieversorgung in Prozent: " + this.getEnergieversorgungInProzent() + 
                "\n" + "Schutzschild in Prozent: " + this.getSchutzschildeInProzent() + "\n" + "Lebenserhaltungssysteme In Prozent: " + 
        		this.getLebenserhaltungssystemeInProzent() +"\n" + "HuelleInProzent: " + this.getHuelleInProzent() + "\n" + 
                "Photonentorpedos: " + this.getPhotonentorpedos() +"\n" + "Reparaturandroid: " + this.getReparaturAndroid());
    }

    /**
     * Ladungsverzeichnis auf der Konsole ausgeben
     */
    public void ladungsVerzeichnisAusgeben() {
    	for(Ladung ausgabe: this.getLadungsverzeichnis()) {
			System.out.println(ausgabe.getBezeichnung() + ": " + ausgabe.getAnzahl());
		}
    }

    public void ladungsVerzeichnisAufraeumen() {
        // TODO
    }
}
