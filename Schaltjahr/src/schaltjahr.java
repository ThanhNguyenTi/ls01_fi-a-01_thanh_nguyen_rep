import java.util.Scanner;

public class schaltjahr {

	public static void main(String[] args) {
		
		Scanner Tastatur = new Scanner(System.in);
		System.out.print("Bitte das Jahr eingeben:");
		int jahr = Tastatur.nextInt();
		
		if (istSchaltjahr(jahr)) {
            System.out.println("Das gegebene Jahr ist ein Schaltjahr");
        } else {
            System.out.println("Das gegeben Jahr ist kein Schaltjahr");
        }
    }
		
		public static boolean istSchaltjahr(int jahr) {
			if (jahr%4 == 0 && (jahr%100 != 0 || jahr%400 == 0)) {
				return true;
			}
			return false;
		}
    }
	


